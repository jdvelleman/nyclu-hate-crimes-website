<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Find Organizations</title>
</head>
<script language='javascript'>
/////////////////////
// NOTE ON QUOTES //
////////////////////
/*

Double quotes could not be entered into text fields, so they are not in database. Apostrophes (hence also single quotes) were replaced by underlines
and stored in that form. Underlines are replaced by apostrophes when data is provided.

*/
var badzipcode = 0
var results = ""
</script>



 
<?php

require('db/NYCLUFUNCTIONS.php');
provide_attribute_list("contextterms","context");
provide_attribute_list("categoryterms","category");


 
if (isset($_POST['zipcode']))
{	provide_search_results($_POST['role'],$_POST['zipcode'],$_POST['getcontext'],$_POST['getcategory'],$_POST['specifics1'],$_POST['specifics2'],$_POST['specifics3'],false);
} else
{ 	if (get_param($action,'action') == "help" && get_param($contact,'contact'))
	{	send_email("Your request for assistance","legalintake@nyclud.org","legalintake@nyclu.org","A complainant has asked to be contacted at ".$contact);
		echo "<script language='javascript'>window.history.back()</script>";
	}
}

?>

<body bgcolor="#E9E8FE">


<table id="main_table" align="center" bgcolor="white">
<tr>
<td id="cell">

  

<form id="searchform" method="post" action="find_orgs.php">

<p>If you have been a victim or witness of discrimination, use this search tool to learn where to file a report and where to find help.
 </p>
<table id="item_table">
<tr>
<td>
1. Enter the zip code where the incident occurred:</td><td>
  <label for="zip"></label>
  <input name="zipcode" type="text" id="zipcode" size="12" value="enter zipcode" onfocus="this.select()"/>
  </td></tr>
    <td>
2. Where you a victim of the incident or only a witness?</td><td>
  	<label for "role"></label>
    <select name="role" id="role" >
    <option value="choose_role">**choose role**</option>
    <option value="victim">victim</option>
    <option value="witness">witness</option>
    </select>
    </td></tr>
    <td>
3.  What was the setting of the incident?</td><td>
   <select name="getcontext" id="getcontext" onChange="checkInstructions(this)">
  </select>
  </td></tr>
    <td>
4. Add specifics about the setting here:</td><td>
  <input name="specifics1" type="text" id="specifics1" size="75" onfocus="this.select()" onchange="unQuote(this)"/>
</td></tr>
    <td>
5. What was the basis of the discrimination?</td><td>
   <select name="getcategory" id="getcategory" onChange="checkInstructions(this)" >
  </select>
 </td></tr>
    <td>
6. Add specifics about the basis of discrimination here:</td><td>
	<input name="specifics2" type="text" id="specifics2" size="75" onfocus="this.select()" onchange="unQuote(this)"/>
</td></tr>
    <td>
7.	What form did the discrimination take?</td><td>
	<input name="specifics3" type="text" id="specifics3" value="+Please describe the incident+" size="75" onfocus="this.select()" onchange="unQuote(this)"/>
   </td></tr>
    <td> 
8. Click here to submit your request:&nbsp;</td><td>
  <input type="button" id="submitbutton" value="GO" onclick="submitMe(this)" />
 </td></tr></table>
</form>


<div id="results_div"></div>
<br /><br />
<div id="button_div"></div>
</td>
</tr>
</table>
</body>
	
    
<script language="javascript">

	context_menu = document.getElementById("getcontext")
	newopt = document.createElement("option")
	newopt.text = "**choose setting**"
	newopt.value = ""
	context_menu.add(newopt)
	for (i=0; i<context_list.length; i++)
	{	newopt = document.createElement("option")
		newopt.text = context_list[i]
		newopt.value = context_list[i]
		context_menu.add(newopt)
	}
	context_menu.add(newopt)
	newopt = document.createElement("option")
	newopt.text = "Other"
	newopt.value = "All"
	context_menu.add(newopt)
	
	category_menu = document.getElementById("getcategory")
	newopt = document.createElement("option")
	newopt.text = "**choose basis**"
	newopt.value = ""
	category_menu.add(newopt)
	for (i=0; i<category_list.length; i++)
	{	newopt = document.createElement("option")
		newopt.text = category_list[i]
		newopt.value = category_list[i]
		category_menu.add(newopt)
	}
	newopt = document.createElement("option")
	newopt.text = "Other"
	newopt.value = "All"
	category_menu.add(newopt)

if (badzipcode == 1) 
{	alert ("That's not a New York State zipcode")
	window.location = "find_orgs.php"
}


	
function checkInstructions(what)
{	if (what.id == "getcontext")
	{	thefield = document.getElementById("specifics1")
		
		if (what.value.indexOf("Education") >= 0 && what.value.indexOf("12") >= 0) thefield.value = "+Please enter the name of the school+"
		  else if (what.value.indexOf("Education") >= 0 && what.value.indexOf("college") >= 0) thefield.value= "+Please enter the name of the college or university+"
			else if (what.value.indexOf("Government Agency") >= 0) thefield.value = "+Please identify the agency+"
			  else if (what.value.indexOf("Organization Receiving") >= 0) thefield.value = "+If you wish, you may identify the organization here+"
				else if (what.value.indexOf("Public Accommodation") >= 0) thefield.value = "+If you wish, you may identify the establishment here+"
				  else if (what.value.indexOf("Residential Facility") >= 0) thefield.value = "+If you wish, you may identify the facility here+"
					else if (what.value.indexOf("Prison") >= 0) thefield.value = "+Please identify the prison+"
					  else if (what.value.indexOf("Jail") >= 0) thefield.value = "+Please identify the jail+"
						else if (what.value.indexOf("Training") >= 0) thefield.value = "+Please identify the school, union, or other training organization+"
						  else if (what.value.indexOf("Commercial") >=0) thefield.value = "+If you wish, you may enter the name and/or location of the business+"
						    else if (what.value.indexOf("Employment") >= 0)thefield.value = "+Please give the name of the employer+"
							  //else if (what.value.indexOf("Health") >=0) thefield.value = "+Please indicate the kind of facility (e.g. hospital) or caregiver (e.g. physician)+"
							    else if (what.value.indexOf("Commercial") >=0) thefield.value = "+Please give the name of the business or owners+"
								  else if (what.value.indexOf("Housing") >=0) thefield.value = "+Please please indicate whether it was public or private housing+"
								    else if (what.value.indexOf("Union") >=0) thefield.value = "+Please give the name of the union+"
									  else if (what.value.indexOf("Conveyance") >=0) thefield.value = "+Please describe the conveyance (city bus, Amtrak, etc.)+"
						  	else thefield.value = ""
							
							thefield.focus()
	}
	
	if (what.id == "getcategory")
	{	thefield = document.getElementById("specifics2")
	
		if (what.value.indexOf("Race") >= 0) thefield.value = "+If you wish, you may identify the race here+"
		  else if (what.value.indexOf("Ethnicity") >= 0) thefield.value = "+If you wish, you may identify the ethnicity here+"
			else if (what.value.indexOf("Disability") >= 0) thefield.value = "+If you wish, you may identify the disability here+"
				else if (what.value.indexOf("Sex") >= 0) thefield.value = "+Indicate here if sexual harassment was involved+"
					else if (what.value.indexOf("Gender Identity") >= 0) thefield.value = "+If you wish, you may identify the gender identity here+"
					  else if (what.value.indexOf("National Origin") >= 0) thefield.value = "+If you wish, you may identify the national origin here+"
						else if (what.value.indexOf("Religion") >= 0) thefield.value = "+If you wish, you may identify the religion here+"
						  else if (what.value.indexOf("Sexual Orientation") >= 0) thefield.value = "+If you wish, you may identify the sexual orientation here+"
							else thefield.value = ""
							
							thefield.focus()
	}
}


	
function submitMe()
{	if (document.getElementById("zipcode").value == "enter zipcode")
	{	alert("Please enter the zipcode where the incident occurred.")
	}	else
	{	if (document.getElementById("role").selectedIndex == "0") 
		{	alert ("Please indicate whether you were a victim or a witness.")		
		}	else 
		{	if (document.getElementById("getcontext").selectedIndex == "0") 
			{	alert ("Please choose the setting where the incident occurred.")			
			}	else 
			{	if (document.getElementById("getcategory").selectedIndex == "0") 
				{	alert ("Please choose the basis of the discrimination.")				
				}	else 
				{	document.getElementById("searchform").submit()
				}
			}
		}
	}
}

function getInstructions(record)
{	alert(eval("results[" + record + "]['info']"))
}

function getHelp()
{	contactinfo = prompt("Enter a telephone number or email address here. A volunteer will contact you.","Enter contact information.")
	if (contactinfo != "")
	{	window.location = "find_orgs.php?action=help&contact=" + contactinfo
	}
}


if (results.length != 0)
{	theparent = document.getElementById("searchform").parentNode	
	i = 0
	while (i < theparent.childNodes.length && theparent.childNodes[i].id != "searchform") i++
	theparent.removeChild(theparent.childNodes[i])
	
	thediv = document.getElementById("results_div")
	
	
	reporting_div = thediv.appendChild(document.createElement("div"))
	reporting_div.id = "reportingresults"
	resource_div = thediv.appendChild(document.createElement("div"))
	resource_div.id = "resourceresults"
	
	thediv = document.getElementById("button_div")
	
	thediv.appendChild(document.createTextNode("To search again, click here: "))
	repeat_button = document.createElement("input")
	repeat_button.type = "button"
	repeat_button.value = "Search Again"
	repeat_button.onclick = function(){window.location = "find_orgs.php"}
	thediv.appendChild(repeat_button)
	
	thediv.appendChild(document.createElement("p"))
	
	thediv.appendChild(document.createTextNode("If you wish to be contacted by an NYCLU volunteer, click here: "))
	help_button = document.createElement("input")
	help_button.type = "button"
	help_button.value = "Help"
	help_button.onclick = function(){getHelp()}
	thediv.appendChild(help_button)
	
	
	
}
							

for (j=1; j<3; j++)
	{	type = ["","reporting","resource"][j]
	    for (i=0; i< results.length; i++)
			{	if (eval("results[" + i + "]['type']") == type)
				 {	if (eval("results[" + i + "]['org']") != '') 
				 	{ 	document.getElementById(type +  "results").innerHTML += reQuote(eval("results[" + i + "]['org']"))
					}
					if (eval("results[" + i + "]['unit']") != '') 
				 	{ 	document.getElementById(type +  "results").innerHTML += "<br />" + reQuote(eval("results[" + i + "]['unit']"))
					}
				 	if (eval("results[" + i + "]['street1']") != '') 
				 	{ 	document.getElementById(type +  "results").innerHTML += "<br />" + reQuote(eval("results[" + i + "]['street1']")) 
					}
					if (eval("results[" + i + "]['street2']") != '') 
				 	{ 	document.getElementById(type +  "results").innerHTML += "<br />" + reQuote(eval("results[" + i + "]['street2']")) 
					}
					if (eval("results[" + i + "]['city']") != '') 
				 	{ 	document.getElementById(type +  "results").innerHTML  += "<br />" + reQuote(eval("results[" + i + "]['city']")) + "&nbsp;" 
					}
					if (eval("results[" + i + "]['state']") != '') 
				 	{ document.getElementById(type +  "results").innerHTML +=  eval("results[" + i + "]['state']") + "&nbsp;"
					}
					if (eval("results[" + i + "]['zip']") != '') 
				 	{ document.getElementById(type +  "results").innerHTML +=  eval("results[" + i + "]['zip']") 
					}
					if (eval("results[" + i + "]['office']") != '') 
				 	{ document.getElementById(type +  "results").innerHTML += "<br />" + eval("results[" + i + "]['office']") 
					}
					if (eval("results[" + i + "]['hotline']") != '') 
				 	{ document.getElementById(type +  "results").innerHTML += "<br />" + eval("results[" + i + "]['hotline']") 
					}
					if (eval("results[" + i + "]['homepage']") != '') 
				 	{	add_this = "<br /><a href='http://" + eval("results[" + i + "]['homepage']") + "' target='new'>home page</a>"
						document.getElementById(type +  "results").innerHTML += add_this
					}
					if (eval("results[" + i + "]['reportpage']") != '') 
				 	{ 	add_this = "<br /><a href='http://" + eval("results[" + i + "]['reportpage']") + "' target='new'>reporting page</a>"
						document.getElementById(type +  "results").innerHTML += add_this
					}
					if (eval("results[" + i + "]['info']") != '') 
				 	{ 	add_this = "<br /><a href='javascript:getInstructions(\"" + i + ") \">information</a>"
						document.getElementById(type +  "results").innerHTML += reQuote(add_this)
					}
					document.getElementById(type +  "results").innerHTML += "<br /><br />"
				}
			} 	
	}

if (results.length != 0 && document.getElementById("reportingresults").innerHTML != "") 
{	reporting_label = document.createElement("div")
	reporting_label.id = "reportinglabel"
	reporting_label.style = "align:center;text-decoration:underline"
	document.getElementById("results_div").insertBefore(reporting_label,document.getElementById("reportingresults"))
	document.getElementById("reportinglabel").innerHTML = "<u><strong>Reporting Agencies</strong></u><br /><br />"
}

if (results.length != 0 && document.getElementById("resourceresults").innerHTML != "") 
{	resource_label = document.createElement("div")
	resource_label.id = "resourcelabel"
	resource_label.style = "align:center;text-decoration:underline"
	document.getElementById("results_div").insertBefore(resource_label,document.getElementById("resourceresults"))
	document.getElementById("resourcelabel").innerHTML = "<u><strong>Resource Organizations</strong></u><br /><br />"
}






	











</script>
</html>

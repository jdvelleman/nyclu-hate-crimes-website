<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Manage Staff</title>
</head>









<body>
<table>
	<tr>
		<td>name</td>
        <td>inits</td>
        <td>region</td>
		<td>email</td>
		<td>phone</td>
        <td>role</td>
	</tr>
		<form id="staffform" method="post" action="manage_staff.php">
			<td><input type="text" size="25" name="name" id="name_field" /></td>
            <td><input type="text" size="4" name="initials" id="initials_field" /></td>
            <td><input type="text" size="25" name="region" id="region_field" /></td>
			<td><input type="text" size="30" name="address" id="email_field" /></td>
			<td><input type="text" size = "13" name="phone" id="phone_field" /></td>
            <td><input type="text" size = "10" name="role" id="role_field" /></td>
            <td><input type="submit" value="submit" /></td>
		</form>
	<tr>
	</tr>
</table>
<table id="staff_table"></table>
</body>

<script language='javascript'>

function testUser(orgnum)
{	passwd = prompt("Please enter your password.")
	window.location = "manage_staff.php?action=getauth&orgnum=" + orgnum	+ "&passwd=" + passwd
}

function testInitials(what)
{	window.location="manage_staff.php?action=initials&value=" + what.value + "&page=" + window.location
}

function challengeUser(orgnum)
{	response = prompt ("That isn't a valid pass code. Click 'Cancel' and try again. Or enter your email address to receive a new code.","[email address]")
	if (response == null) 
	{	testUser(orgnum)
	}	else
	{	if (response.indexOf("@" >= 0))
		{	window.location = "manage_staff.php?action=newcode&email=" + response
		}	else
		{	prompt ("That isn't a valid email address. Click 'Cancel' and try your password again. Or enter your email address to receive a new code.","[email address]")
			testUser()
		}
	}
}
</script>
<?php

require('db/NYCLUFUNCTIONS.php');

if (get_param($action,'action'))
{	if ($action == "change" && get_param($field,'field') && get_param($value,'value') && get_param($email,'email'))
	{	change_staff_field($field,$value,$email);
	} else
	{	if ($action == "drop" && get_param($email,'email')) delete_staff($email); 
		else 
		{	if ($action == "getauth" && get_param($passwd,'passwd')) 
			{	get_authorization($passwd,"xxxxxx");
				provide_staff();
			}
		}
	}
} else
{	if (isset($_POST['name']))
	{	add_staff($_POST['name'],$_POST['initials'],$_POST['region'],$_POST['address'],$_POST['phone']);
	} else
	{	get_authorization("begin","xxxxxx");
		provide_staff();
	}
	
}



?>

<script language='javascript'>

function changeField(which)
{	therow = which.id.slice(which.id.length-1)
	field = which.id.slice(5,which.id.length-1)
	value = document.getElementById(which.id).value
	email = document.getElementById("next_email" + therow).value
	window.location = "manage_staff.php?action=change&field=" + field + "&value=" + value + "&email=" + email
}

function deleteStaff(email)
{	window.location = "manage_staff.php?action=drop&email=" + email
}
</script>



</html>
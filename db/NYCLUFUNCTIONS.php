<?php
/*
The tables in the database are as follows:

orgs: Usually one record per agency
	ID number ("orgnum") for the agency
	Contact information for the agency
	URLs for the agency
	Brief description of the agency
	Designation as "reporting" or "resource" agency
	True/false notation for "no witnesses"
	Date added to the database
	Person who added it
	Person who approved ("checked") it
	Last date the record was changed.

ziptocounty: <zipcode,town,county>
	A table of NYS zipcodes matched with towns and counties

categoryterms: the list of terms for protected categories

contextterms: the list of terms for regulated areas

staff: <name,initials,email,phone,passwd,role>
	the roster of people authorized to word on the database, 
	with password, email address roles

contexts: <orgnum,context,keyword>
	One record for each combination of context & keyword that
	the org covers.
	
categorys: <orgnum,category,keyword>
	One record for each combination of category & keyword that
	the org covers. Note (mis-)spelling.
	
zipcodes: <orgnum,zipcode>
	One record for each zipcode the org covers,
	if its coverage area is defined by zipcode.
	
countys: <orgnum,county>
	One record for each county the org covers,
	if its coverage area is defined by larger regions.
	("Nation-wide" and "State-wide" are on the list of counties)
	
searches: <date,zipcode,context,category,role (witness/victim),
	specifics1 (context description), specifics2 (category description)
	specifics3 (incident description)>
	The log of searches performed with find_orgs.php
	
clock: <initials, ip address, UNIX timestamp>
	The initials of the person who authenticated from the
	ip address at the Unix time. Tracks session of
	authenticated user.

*/
function mysqli_result($res,$row=0,$col=0){ 
    $numrows = mysqli_num_rows($res); 
    if ($numrows && $row <= ($numrows-1) && $row >=0){
        mysqli_data_seek($res,$row);
        $resrow = (is_numeric($col)) ? mysqli_fetch_row($res) : mysqli_fetch_assoc($res);
        if (isset($resrow[$col])){
            return $resrow[$col];
        }
    }
    return false;
}


define('NEWLINE', "\n");

function db_connect()
{	$db = mysqli_connect('localhost', 'biasrepo_davidv', 'Applebaum!1','biasrepo_data');   

	return $db;

	
	/*if (!$connect) 	
	{	return -2;
	}
	return true;
	*/
}

function get_param(&$variable, $parameter)	//pull URL parameters into a given variable
{	
	if (isset($_GET[$parameter]))		//if we were given an argument named $parameter in the URL...
	{	$variable = $_GET[$parameter];	//...then put it into the variable we were given...
		return true;						//...and return successfully.
	}
	else return false;						//otherwise, do nothing and return a failure.
}	
	

//###############################//
//    MySQL ADD FUNCTIONS        //
//###############################//


function add_org($org,$type) // Called by edit_orgs.php. Adds an agency to the database. 
{	$db = db_connect();
	get_authorization("begin","000000");		//makes sure the user is on the clock.
	$user = get_user();		//gets initials of user at this ip address (who may not have needed to authenticate)
	$orgnum = get_unique_id();			// Generates a unique id number for the agency.
	$query = "INSERT INTO orgs (org,orgnum,added_by,type,date_added) VALUES ";
	$query .= "('".$org."','".$orgnum."','".$user."','".$type."','".date("d/m/y")."')";
	$result = mysqli_query($db,$query);
	provide_org("orgnum",$orgnum);		//If authorization failed, there's no org to provide.
}

function add_search($role,$zipcode,$context,$category,$specifics1,$specifics2,$specifics3) // Adds the details of a user's search to the log of searches. 
{	$db = db_connect();
	$query = "INSERT into searches (date,role,zipcode,context,category,specifics1,specifics2,specifics3) VALUES ('".date("y/m/d")."','".$role."','".$zipcode."','".$context."','";
	$query .= $category."','".$specifics1."','".$specifics2."','".$specifics3."')";
	$result = mysqli_query($db,$query);
}

function add_specifics($orgnum,$field,$value,$keyword)	//Adds specifiers to the orgnum,value pairs in the contexts or categories tables.
{	$db = db_connect();
	get_authorization("begin",$orgnum);
	if ($keyword != "keyword")
	{	$query0 = "DELETE FROM ".$field."s WHERE orgnum = '".$orgnum."' AND ".$field." = '".$value."' AND (specifier IS NULL or specifier = '')";
		$result0 = mysqli_query($db,$query0);
		$query1 = "INSERT INTO ".$field."s (orgnum,".$field.",specifier) VALUES ( '".$orgnum."','".$value."','".$keyword."')";
		$result1 = mysqli_query($db,$query1);
	}

	provide_org('orgnum',$orgnum);						//Was called by edit_orgs; now updates the page
}

function add_staff($name,$initials,$region,$email,$phone)	// Adds a staff member to the table staff. 
{	$db = db_connect();
	get_authorization("begin","000000");
	$region = str_replace("'","`",$region);			//makes sure there are no quotation marks in the region
	$region = str_replace('"','`',$region);
	$query1 = "SELECT * FROM staff WHERE email = '".$email."'";
	$result = mysqli_query($db,$query1);
	$passwd = get_passwd();
	if (mysqli_num_rows($result) > 0) echo "<script language='javascript'>alert ('Duplicate!')</script>"; // Makes sure the person isn't already in the table
	else
	{	
		$query2 = "INSERT INTO staff (name, initials, passwd, region, email, phone) VALUES ('".$name."','".$initials."','".$passwd."','".$region."','".$email."','".$phone."')";
		$result = mysqli_query($db,$query2);
		$message = "Dear ".$name.",".NEWLINE.NEWLINE;
		$message .= "You have been granted access to the NYCLU's database of agencies dealing with bias-based incidents. This email will ";
		$message .= "explain how to get started. ".NEWLINE.NEWLINE;
		$message .= "First, point your browser to biasreportinghelp.org/instructions.html and follow the instructions ";
		$message .= "for accessing and using the search page, as if you were a member of the public looking for help in reporting an incident of bias. ";
		$message .= "That will give you an idea of how the data you enter as a volunteer will be passed along to users. ";
		$message .= "(Note: the search page will look different when we release it to the public. This is just a mock-up.)".NEWLINE.NEWLINE;
		$message .= "After running one or two searches, follow the instructions for signing on to the database, at a different web address. ";
		$message .= "You will need a password to sign on. Your personal password is: ".$passwd;
		$message .= ". If you lose or forget your password, you can receive a new one by email: just enter a ";
		$message .= "guess into the 'passcode' field, hit the 'tab' key, and you will be prompted to supply your email address.".NEWLINE.NEWLINE;
		$message .= "Now follow the instructions for entering an organization into the database. ";
		$message .= "After you have entered your first one, please email the organization's name to us at: admin@biasreportinghelp.org . ";
		$message .= "Don't keep going until we have had a chance to look at your first try.  "; 
		$message .= "We will take a quick look and send you the go-ahead to continue. ".NEWLINE.NEWLINE;
		$message .= "You can use the same email address for questions and problems. The software may still have bugs, ";
		$message .= "so if you run into trouble, don't assume it's your fault: shoot us an email.".NEWLINE.NEWLINE;
		$message .= "Thanks for participating in this project.".NEWLINE.NEWLINE."The Tech Team";
		send_email($email,'NYCLU Bias Incident Reporting Project',$message);
	}
	provide_staff();
}

function add_term($field,$term)			//	Adds a term to the tables contextterms and categoryterms -- lists of the terms that show up in context and category menus 
{	$db = db_connect();
	get_authorization("begin","000000");
	$query = "INSERT INTO ".$field."terms (".$field.") VALUES ('".$term."')";
	echo $query;
	$result = mysqli_query($db,$query);
	provide_terms();
}

function add_value($orgnum,$field,$value)	// Adds an <orgum,value> pair to the countys, zipcodes, contexts, or categorys tables 
{	$db = db_connect();
	get_authorization("begin",$orgnum);
	if ($field == "county" && $value == "city")
	{	$city_counties = array('Bronx','Kings','New York','Queens','Richmond');
		for ($i=0; $i<count($city_counties); $i++)
		{	$query = "INSERT INTO countys (orgnum,county) VALUES ('".$orgnum."','".$city_counties[$i]."')"; 
			$result = mysqli_query($db,$query);
		}
	} else 
	{	$query = "INSERT into ".$field."s (orgnum,".$field.") VALUES ('".$orgnum."','".$value."')";
		$result = mysqli_query($db,$query);
	}
	provide_org('orgnum',$orgnum);		//Was called by edit_orgs; now updates the page
}



//###############################//
//    MySQL GET FUNCTIONS        //
//###############################//

function get_any_id()					//	returns a random six-digit number from 000001 to 999999, which is checked for uniqueness by get_unique_id
{	$orgnum = strval(rand(1,999999));
	while (strlen($orgnum) < 6) $orgnum = "0".$orgnum;
	return $orgnum;
}

function get_attributes($orgnum,$field)	//Gets a column of values associated with an agency. 
{	$db = db_connect(); 
	$query = "SELECT * FROM `".$field."s` WHERE orgnum = '".$orgnum."' ORDER BY ".$field;  //tables are named "zipcodes", "countys", "contexts","categorys" 
	$result = mysqli_query($db,$query);															//columns are nameed "zipcode","county","context","category"
	return $result;
}

function get_attribute_list($table,$attribute)		// Gets a list of the values that appear in a table -- i.e., the countys, contexts, or categories
{	$db = db_connect();
	$query = "SELECT DISTINCT(`".$attribute."`) from `".$table."` ORDER BY `".$attribute."`";
	$result= mysqli_query($db,$query);
	return $result;
}

function get_author($orgnum)
{	$query = "SELECT added_by from orgs WHERE orgnum = '".$orgnum."'";
	$result = mysqli_query($db,$query);
	return mysqli_result($result,0,'added_by');
}

function get_auth_orgs($author,$direction)
{	if ($direction == 'next') $query = "SELECT * FROM orgs WHERE added_by = '".$author."' ORDER BY date_added"; //Get the orgs entered by this author, in date order...
	else $query = "SELECT * FROM orgs WHERE added_by = '".$author."' ORDER BY date_added DESC";					//...or reverse order
	$result = mysqli_query($db,$query);
	return $result;
}

function get_authorization($passwd,$orgnum)	//called by add_org(), change_field(), add_term(), delete_term(), add_specifics(), add_value(), delete_value(	
{	$db = db_connect();
	$location = $_SERVER['REMOTE_ADDR'];
	$cutoff = time()-3600;														//clear the clock of entries older than 1 hr.
	$query5 = "DELETE FROM clock WHERE seconds < ".$cutoff;
	$result = mysqli_query($db,$query5);
	if ($passwd == "begin")																								//If we haven't been offered a password ...
	{	$query0 = "SELECT * FROM clock WHERE location = '".$location."'"; 													//... look for the IP address on the clock.
		$result0 = mysqli_query($db,$query0);
		if (mysqli_num_rows($result0) == 0)																					//If it's not there...
		{	echo "<script language='javascript'>testUser('".$orgnum."')</script>";											//...ask for a password
			return false;																									
		}	else 																											//If it is there...
		{	return true;																									// ... issue a pass.	
		}
	}	else																											//If we've been given a password ...													
	{	
		$query1 = "SELECT * FROM staff WHERE passwd = '".$passwd."'";    	
		$result = mysqli_query($db,$query1);
		if (mysqli_num_rows($result) == 0)																					// ... test whether the password valid...		
		{ 	echo "<script language='javascript'>challengeUser('".$orgnum."')</script>";
			return false;									// ... if not, issue a challenge.
		}	else																												 // If the password is valid...	
		{	$initials = mysqli_result($result,0,'initials');																	 //...get the owner's initials...										
			$query3 = "INSERT INTO clock (initials,location,seconds) VALUES ('".$initials."','".$location."',UNIX_TIMESTAMP())"; // ... and put him on the clock
			$result3 = mysqli_query($db,$query3);
			return true;																				
		}
	}
}

function get_checker($orgnum)
{	$query = "SELECT checker from orgs WHERE orgnum = '".$orgnum."'";
	$result = mysqli_query($db,$query);
	return mysqli_result($result,0,'checker');
}

function get_county($zipcode)
{	$db = db_connect();
	$query="SELECT * FROM ziptocounty WHERE zipcode = '".$zipcode."'";
	$result= mysqli_query($db,$query);
	if (mysqli_num_rows($result) < 1)
	{	echo "<script language='javascript'>badzipcode = 1</script>";									//If the user reports a non-valid zipcode, this will tell find_orgs.php
	} 	else																							//to give him an error message.
	{	$county = mysqli_result($result,0,'county');														//Otherwise, the county is returned.
		return $county;
	}
}

function get_first_org_num($author)
{	$db = db_connect();
	$query = "SELECT * FROM orgs ";
	if ($author != "noauthor") $query .= "WHERE added_by = '".$author."' ORDER BY date_added DESC";
	else $query .= "ORDER BY org";
	$result = mysqli_query($db,$query);
	return mysqli_result($result,0,"orgnum");
}

function get_org($field,$value)
{	$db = db_connect();
	$query = "SELECT * FROM orgs WHERE `".$field."` LIKE '".$value."' ORDER BY org";
	$result = mysqli_query($db,$query);
	return $result;
}

function get_passwd()																					//Gets a unique random string of 6 characters.
{	$db = db_connect();
	$str = '';
    $a = "abcdefghijklmnopqrstuvwxyz0123456789?!";
    $b = str_split($a);
    for ($i=1; $i <= 6 ; $i++) $str .= $b[rand(0,strlen($a)-1)];
	$query = "SELECT * FROM staff WHERE passwd = '".$str."'";
	$result = mysqli_query($db,$query);
	if (mysqli_num_rows($result) > 0) get_passwd();
    return $str;
}

function get_query_string($queries)
{	$queries = explode(",",$queries);
	$thestring = "";
	$i = 0;
	while ($i < 10)
	{	$thestring .= $queries[$i]."=".$queries[$i+1]."&";
		$i = $i + 2;
	}
	return $thestring;
}

function get_searches()
{	$db = db_connect();
	$query = "SELECT * FROM searches ORDER BY date DESC";
	$result = mysqli_query($db,$query);
	return $result;
}

function get_search_results($zipcode,$context,$category,$role,$specifier1,$specifier2)  // The search query called by find_orgs.php. $context and $category may be either a singe term or a 2-term pair, 
{	$db = db_connect();						
	$searchcounty = get_county($zipcode);				// $zipcode is the zip in which the reported incident occured. get_county finds the county it is in.
	
	$query1 = "SELECT contexts.orgnum FROM contexts,categorys WHERE contexts.orgnum = categorys.orgnum "; //Four search queries are defined. Each looks for orgs that either
	$query1 .= "AND (contexts.context = '".$context."' OR contexts.context = 'All') ";					  //match the requested context and category or are non-specific in those
	$query1 .= "AND (contexts.specifier IS NULL OR contexts.specifier LIKE '%".$specifier1."%' OR '";
	$query1 .= $specifier1."' LIKE CONCAT ('%',contexts.specifier,'%')) ";  
	$query1 .= "AND (categorys.category = '".$category."' OR categorys.category = 'All') ";	
	$query1 .= "AND (categorys.specifier IS NULL OR  categorys.specifier LIKE '%".$specifier2."%' OR '";
	$query1 .= $specifier2."' LIKE CONCAT ('%',categorys.specifier,'%')) ";  
	$query1 .= "AND contexts.orgnum NOT IN (SELECT orgnum FROM orgs WHERE checker IS NULL OR checker =''";			//respects. This search narrows the results down to zipcode matches...
	if ($role == "witness") $query1 .= " OR nowitnesses = 'true') ";
	else $query1 .= ") ";
	$query1 .= "AND contexts.orgnum IN (SELECT orgnum FROM zipcodes WHERE zipcode = '".$zipcode."')";
	
	$query2= "SELECT contexts.orgnum FROM contexts,categorys WHERE contexts.orgnum = categorys.orgnum ";
	$query2 .= "AND (contexts.context = '".$context."' OR contexts.context = 'All') ";
	$query2 .= "AND (contexts.specifier IS NULL OR contexts.specifier LIKE '%".$specifier1."%' OR '";
	$query2 .= $specifier1."' LIKE CONCAT ('%',contexts.specifier,'%')) "; 
	$query2 .= "AND (categorys.category = '".$category."' OR categorys.category = 'All') ";
	$query2 .= "AND (categorys.specifier IS NULL OR  categorys.specifier LIKE '%".$specifier2."%' OR '";
	$query2 .= $specifier2."' LIKE CONCAT ('%',categorys.specifier,'%')) "; 
	$query2 .= "AND contexts.orgnum NOT IN (SELECT orgnum FROM orgs WHERE checker IS NULL OR checker =''";			//respects. This search narrows the results down to zipcode matches...
	if ($role == "witness") $query2 .= " OR nowitnesses = 'true') ";
	else $query2 .= ") ";
	$query2 .= "AND contexts.orgnum IN (SELECT orgnum FROM countys WHERE county = '".$searchcounty."')";// ... county matches...
	
	$query3 = "SELECT contexts.orgnum FROM contexts,categorys WHERE contexts.orgnum = categorys.orgnum ";
	$query3 .= "AND (contexts.context = '".$context."' OR contexts.context = 'All') ";
	$query3 .= "AND (contexts.specifier IS NULL OR contexts.specifier LIKE '%".$specifier1."%' OR '";;
	$query3 .= $specifier1."' LIKE CONCAT ('%',contexts.specifier,'%')) ";   
	$query3 .= "AND (categorys.category = '".$category."' OR categorys.category = 'All') ";
	$query3 .= "AND (categorys.specifier IS NULL OR  categorys.specifier LIKE '%".$specifier2."%' OR '";
	$query3 .= $specifier2."' LIKE CONCAT ('%',categorys.specifier,'%')) "; 
	$query3 .= "AND contexts.orgnum NOT IN (SELECT orgnum FROM orgs WHERE checker IS NULL OR checker =''";			//respects. This search narrows the results down to zipcode matches...
	if ($role == "witness") $query3 .= " OR nowitnesses = 'true') ";
	else $query3 .= ") ";	
	$query3 .= "AND contexts.orgnum IN (SELECT orgnum FROM countys WHERE county = 'state')";		   // ... orgs that are state-wide...
	
	$query4 = "SELECT contexts.orgnum FROM contexts,categorys WHERE contexts.orgnum = categorys.orgnum ";
	$query4 .= "AND (contexts.context = '".$context."' OR contexts.context = 'All') ";
	$query4 .= "AND (contexts.specifier IS NULL OR contexts.specifier LIKE '%".$specifier1."%' OR '";;
	$query4 .= $specifier1."' LIKE CONCAT ('%',contexts.specifier,'%')) "; 
	$query4 .= "AND (categorys.category = '".$category."' OR categorys.category = 'All') ";
	$query4 .= "AND (categorys.specifier IS NULL OR  categorys.specifier LIKE '%".$specifier2."%' OR '";
	$query4 .= $specifier2."' LIKE CONCAT ('%',categorys.specifier,'%')) "; 
	$query4 .= "AND contexts.orgnum NOT IN (SELECT orgnum FROM orgs WHERE checker IS NULL OR checker =''";			//respects. This search narrows the results down to zipcode matches...
	if ($role == "witness") $query4 .= " OR nowitnesses = 'true') ";
	else $query4 .= ") ";	
	$query4 .= "AND contexts.orgnum IN (SELECT orgnum FROM countys WHERE county = 'nation')";     		//...orgs that are nation-wide. 
	
	$final_query = '('.$query1.') UNION ('.$query2.') UNION ('.$query3.') UNION ('.$query4.')';
	$final_result = mysqli_query($db,$final_query); //The union of these searches will return results in that order, zipcode matches
	//echo $final_query;
	return $final_result;																					 //first, then county matches, etc.
	
}


function get_unique_id()							// returns a unique referee_key (i.e., six-digit number not current used as a referee_key
{	$db = db_connect();
	$orgnum = get_any_id();
	$query = "Select * FROM orgs WHERE orgnum = '".$orgnum."'";
	$result = mysqli_query($db,$query);
	while (mysqli_num_rows($result) > 0)				// keep getting a new key until it doesn't match an existing one.
	{	$orgnum = get_any_id();
		$query = "Select * FROM orgs WHERE orgnum = '".$orgnum."'";
		$result = mysqli_query($db,$query);
	}	
	return $orgnum;
} 

function get_user()
{	$db = db_connect();
	$location = $_SERVER['REMOTE_ADDR'];
	$query = "SELECT * FROM clock WHERE location = '".$location."'";
	$result = mysqli_query($db,$query);
	return mysqli_result($result,0,'initials');
}

function get_user_role($user)
{	$db = db_connect();
	$query = "SELECT * FROM staff WHERE initials = '".$user."'";
	$result = mysqli_query($db,$query);
	$role = mysqli_result($result,0,'role');
	return $role;
}


//###############################//
//    MySQL PROVIDE FUNCTIONS    //
//###############################//

function provide_another_org($orgnum,$order)								//provides the next or previous org in the orgs table.
{	$db = db_connect();
	//if ($order == "next") $query = "SELECT * FROM orgs ORDER BY org";
	//else $query = "SELECT * FROM orgs ORDER BY org DESC";
	$query = "SELECT * FROM orgs ORDER BY org";
	$result = mysqli_query($db,$query);
	if ($order == "next")
	{	$i = 0; 
		while (mysqli_result($result,$i,'orgnum') != $orgnum && $i <= mysqli_num_rows($result) - 1) $i++;
		if ($i < mysqli_num_rows($result) - 1) $i++;
		provide_org('orgnum',mysqli_result($result,$i,'orgnum'));
	} else
	{	$i = mysqli_num_rows($result)-1;
		while (mysqli_result($result,$i,'orgnum') != $orgnum && $i > 0) $i--;
		if ($i > 0) $i--;
		provide_org('orgnum',mysqli_result($result,$i,'orgnum'));
	}
}

function provide_another_auth_org($author,$orgnum,$direction)				//provides the next or previous org entered by this author (in reverse date of entry)
{	$db = db_connect();
	$result = get_auth_orgs($author,$direction);
	echo mysqli_num_rows($result);
	$i = 0;
	while (mysqli_result($result,$i,'orgnum') != $orgnum && $i<mysqli_num_rows($result)-1) $i++; //Find the currect org in the list
	if ($i<(mysqli_num_rows($result)-1)) $i++;
	provide_org('orgnum',mysqli_result($result,$i,'orgnum'));								//and provide the next one.
}



function provide_attribute_list($table,$attribute)		//provides lists of counties, contexts, and categories for the menus on edit_orgs.php and find_orgs.php
{	$record = get_attribute_list($table,$attribute);
	echo "<script language='javascript'>".NEWLINE;
	echo $attribute."_list= new Array()".NEWLINE;
	for ($i=0; $i<mysqli_num_rows($record); $i++)
	{	echo $attribute."_list[".$i."] = '".mysqli_result($record,$i,$attribute)."'".NEWLINE;
	}
	echo "</script>".NEWLINE;
}

function provide_county($zipcode)
{	$county = get_county($zipcode);
	echo "<script language='javascript'>".NEWLINE."var zipcode = '".$zipcode."'".NEWLINE."var county = '".$county."'".NEWLINE."</script>".NEWLINE;
		
}

function provide_first_org()
{	get_authorization("begin","000000");	
	$orgnum = get_first_org_num("noauthor");
	provide_org('orgnum',$orgnum);
}

function provide_org($field,$value)						//provides all the info on an agency to edit_orgs.php					
{	
	$record = get_org($field,$value);	
	$orgnum = mysqli_result($record,0,'orgnum');
	echo "<script language='javascript'>".NEWLINE;
	echo "var v_orgnum = '".$orgnum."'".NEWLINE;
	echo "var v_org = '".mysqli_result($record,0,'org')."'".NEWLINE;
	echo "var v_unit = '".mysqli_result($record,0,'unit')."'".NEWLINE;
	echo "var v_street1 = '".mysqli_result($record,0,'street1')."'".NEWLINE;
	echo "var v_street2 = '".mysqli_result($record,0,'street2')."'".NEWLINE;
	echo "var v_city = '".mysqli_result($record,0,'city')."'".NEWLINE;
	echo "var v_state = '".mysqli_result($record,0,'state')."'".NEWLINE;
	echo "var v_zip = '".mysqli_result($record,0,'zip')."'".NEWLINE;
	echo "var v_office = '".mysqli_result($record,0,'office')."'".NEWLINE;
	echo "var v_hotline ='".mysqli_result($record,0,'hotline')."'".NEWLINE;
	echo "var v_fax = '".mysqli_result($record,0,'fax')."'".NEWLINE;
	echo "var v_homepage = '".mysqli_result($record,0,'homepage')."'".NEWLINE;
	echo "var v_reportpage = '".mysqli_result($record,0,'reportpage')."'".NEWLINE;
	echo "var v_type = '".mysqli_result($record,0,'type')."'".NEWLINE;
	echo "var v_nowitnesses = '".mysqli_result($record,0,'nowitnesses')."'".NEWLINE;
	echo "var v_author = '".mysqli_result($record,0,'added_by')."'".NEWLINE;
	echo "var v_checker = '".mysqli_result($record,0,'checker')."'".NEWLINE;
	echo "var v_date = '".mysqli_result($record,0,'date_changed')."'".NEWLINE;
	echo "var v_info = '".mysqli_result($record,0,'info')."'".NEWLINE;
	
	
	echo "zipcodes = new Array()".NEWLINE;				//the zipcodes associated with the agency OR...
	$zipcodes = get_attributes($orgnum,'zipcode');
	for ($k=0; $k<mysqli_num_rows($zipcodes); $k++)
	{ 	echo "zipcodes[".$k."] = '".mysqli_result($zipcodes,$k,'zipcode')."'".NEWLINE;
	}
	
	echo "countys = new Array()".NEWLINE;				//... the counties associated with the agency (there won't be both)
	$countys = get_attributes($orgnum,'county');
	for ($i=0; $i<mysqli_num_rows($countys); $i++)
	{ 	echo "countys[".$i."] = '".mysqli_result($countys,$i,'county')."'".NEWLINE;
	}
	
	echo "contexts = new Array()".NEWLINE;				//the contexts associated with the agency
	$contexts = get_attributes($orgnum,'context');
	for ($j=0; $j<mysqli_num_rows($contexts); $j++)
	{ 	if (mysqli_result($contexts,$j,'specifier') != "")
		{	echo "contexts[".$j."] = ['".mysqli_result($contexts,$j,'context')."','".mysqli_result($contexts,$j,'specifier')."']".NEWLINE;
		} 	else
		{	echo "contexts[".$j."] = '".mysqli_result($contexts,$j,'context')."'".NEWLINE;
		}
	}
	
	echo "categorys = new Array()".NEWLINE;				//the categories associated with the agency
	$categorys = get_attributes($orgnum,'category');
	for ($j=0; $j<mysqli_num_rows($categorys); $j++)
	{ 	if (mysqli_result($categorys,$j,'specifier') != "")
		{		echo "categorys[".$j."] = ['".mysqli_result($categorys,$j,'category')."','".mysqli_result($categorys,$j,'specifier')."']".NEWLINE;
		} 	else
		{	echo "categorys[".$j."] = '".mysqli_result($categorys,$j,'category')."'".NEWLINE;
		}
	}
	echo "</script>".NEWLINE;

}



function provide_org_list()								//called by listwindow.php; provides links to the agencies, to appear 
{	$db = db_connect();
	get_authorization("begin","xxxxxx");
	$query = "SELECT * FROM orgs ORDER BY org";
	$result = mysqli_query($db,$query);
	echo "<script language='javascript'>".NEWLINE;
	echo "org_list = new Array()".NEWLINE;
	for ($i=0; $i<mysqli_num_rows($result); $i++)
	{	$next_org = "org_list[".$i."] = new Array('".mysqli_result($result,$i,'org')."','".mysqli_result($result,$i,'unit')."','";
		$next_org .= mysqli_result($result,$i,'orgnum')."')".NEWLINE;
		echo $next_org;
	}
	echo "</script>";
}

function provide_searches()													//provides the log of searches that have been performed
{	$db = db_connect();
	get_authorization("begin","xxxxxx");
	$result = get_searches();
	echo "<script language='javascript'>".NEWLINE;
	echo "searches = new Array()".NEWLINE;
	for ($i=0; $i<mysqli_num_rows($result); $i++)
	{	echo "searches[".$i."] = new Array()".NEWLINE;
		echo "searches[".$i."]['date'] = '".mysqli_result($result,$i,'date')."'".NEWLINE;
		echo "searches[".$i."]['role'] = '".mysqli_result($result,$i,'role')."'".NEWLINE;
		echo "searches[".$i."]['zipcode'] = '".mysqli_result($result,$i,'zipcode')."'".NEWLINE;
		echo "searches[".$i."]['context'] = '".mysqli_result($result,$i,'context')."'".NEWLINE;
		echo "searches[".$i."]['specifics1'] = '".mysqli_result($result,$i,'specifics1')."'".NEWLINE;
		echo "searches[".$i."]['category'] = '".mysqli_result($result,$i,'category')."'".NEWLINE;
		echo "searches[".$i."]['specifics2'] = '".mysqli_result($result,$i,'specifics2')."'".NEWLINE;
		echo "searches[".$i."]['specifics3'] = '".mysqli_result($result,$i,'specifics3')."'".NEWLINE;
	}
	echo "fillTable()".NEWLINE."</script>"; 
}

function provide_search_results($role,$zipcode,$context,$category,$specifics1,$specifics2,$specifics3,$test) // called by the search form on find_orgs.php												
{	$db = db_connect();

	if (strspn($specifics1,"+") > 0) $specifics1 = "xxxxxx";	
	if 	(strspn($specifics2,"+") > 0) $specifics2 = "xxxxxx";
	if 	(strspn($specifics3,"+") > 0) $specifics3 = "";

	$search_results = get_search_results($zipcode,$context,$category,$role,$specifics1,$specifics2); //get_search_results returns a list of orgnums that match county, context,category.
	$to_echo = "<script language='javascript'>".NEWLINE;																						
	
	if (mysqli_num_rows($search_results) == 0) $to_echo .= "alert('There were no results for that search.')";
	else
	{	$to_echo .=  "results = new Array()".NEWLINE;																	
		for ($i=0; $i<mysqli_num_rows($search_results); $i++)																				
		{	$to_echo .=  "results[".$i."] = new Array()".NEWLINE;							
			$orgnum = mysqli_result($search_results,$i,'orgnum');	
			if (!!stripos($to_echo,$orgnum)) continue;								//if the organization is already 
			
			$query = "SELECT * FROM orgs WHERE orgnum = '".$orgnum."'";									
			$result = mysqli_query($db,$query);
			
		
			$to_echo .=  "results[".$i."]['org'] = '".mysqli_result($result,0,'org')."'".NEWLINE;				//aills the sub-array in find_orgs.php
			$to_echo .=  "results[".$i."]['orgnum'] = '".mysqli_result($result,0,'orgnum')."'".NEWLINE;
			$to_echo .=  "results[".$i."]['unit'] = '".mysqli_result($result,0,'unit')."'".NEWLINE;
			$to_echo .=  "results[".$i."]['type'] = '".mysqli_result($result,0,'type')."'".NEWLINE;
			$to_echo .=  "results[".$i."]['street1'] = '".mysqli_result($result,0,'street1')."'".NEWLINE;
			$to_echo .=  "results[".$i."]['street2'] = '".mysqli_result($result,0,'street2')."'".NEWLINE;
			$to_echo .=  "results[".$i."]['city'] = '".mysqli_result($result,0,'city')."'".NEWLINE;
			$to_echo .=  "results[".$i."]['state'] = '".mysqli_result($result,0,'state')."'".NEWLINE;
			$to_echo .=  "results[".$i."]['zip'] = '".mysqli_result($result,0,'zip')."'".NEWLINE;
			$to_echo .=  "results[".$i."]['office'] = '".mysqli_result($result,0,'office')."'".NEWLINE;
			$to_echo .=  "results[".$i."]['hotline'] = '".mysqli_result($result,0,'hotline')."'".NEWLINE;
			$to_echo .=  "results[".$i."]['fax'] = '".mysqli_result($result,0,'fax')."'".NEWLINE;
			$to_echo .=  "results[".$i."]['homepage'] = '".mysqli_result($result,0,'homepage')."'".NEWLINE;
			$to_echo .=  "results[".$i."]['reportpage'] = '".mysqli_result($result,0,'reportpage')."'".NEWLINE;	
			$to_echo .=  "results[".$i."]['info'] = '".mysqli_result($result,0,'info')."'".NEWLINE;
		}
	}
	
	$to_echo .= "</script>";
	echo $to_echo;
	if (!$test) add_search($role,$zipcode,$context,$category,$specifics1,$specifics2,$specifics3);		//adds the search details to the log.
}

function provide_staff()												// provides the roster of staff
{	$db = db_connect();
	//get_authorization("begin","xxxxxx");	
	$query = "SELECT * FROM staff";
	$result = mysqli_query($db,$query);
	
	echo "<script language='javascript'>".NEWLINE;
	for ($i=0; $i<mysqli_num_rows($result); $i++)
	{	if ($i % 2 == 0) $new_row = "<tr bgcolor='yellow'>";				// alternates color of the rows
		else $new_row = "<tr bgcolor = 'pink'>";
		$name = mysqli_result($result,$i,'name');
		$initials = mysqli_result($result,$i,'initials');
		$region = mysqli_result($result,$i,'region');
		$email = mysqli_result($result,$i,'email');
		$phone = mysqli_result($result,$i,'phone');
		$role = mysqli_result($result,$i,'role');
		$new_row .= "<td><input type='text' size='25' id='next_name".$i."' value='".$name."' onChange='changeField(this)' /></td>";
		$new_row .= "<td><input type='text' size='4' id='next_initials".$i."' value='".$initials."' onChange='changeField(this)' /></td>";
		$new_row .= "<td><input type='text' size='25' id='next_region".$i."' value='".$region."' onChange='changeField(this)' /></td>";
		$new_row .= "<td><input type='text' size='30' id='next_email".$i."' value='".$email."' onChange='changeField(this)' /></td>";
		$new_row .= "<td><input type='text' size='13' id='next_phone".$i."' value='".$phone."' onChange='changeField(this)' /></td>";
		$new_row .= "<td><input type='text' size='10' id='next_role".$i."' value='".$role."' onChange='changeField(this)' /></td>";
		$new_row .= "<td><input type='button' value='Delete' onclick='deleteStaff(\\\"".$email."\\\")' /></td></tr>".'"'.NEWLINE;
		echo "document.getElementById('staff_table').innerHTML += ".'"'.$new_row;		
	}
	echo "</script>";
}

function provide_terms()
{	get_authorization("begin","xxxxxx");
	provide_attribute_list("contexts","context");
	provide_attribute_list("categorys","category");
}

function provide_unchecked()
{	$db = db_connect();
	$query = "SELECT * FROM orgs WHERE checker IS NULL OR checker = ''";						//provides the next org that hasn't been checked
	$result = mysqli_query($db,$query);
	provide_org('orgnum',mysqli_result($result,0,'orgnum'));
}

//###############################//
//    MySQL CHANGE FUNCTIONS    //
//###############################//

function change_field($orgnum,$field,$value,$next)			
{	$db = db_connect();									
	get_authorization("begin",$orgnum);																
	if ($field == "checker")									//If user is trying to enter initials into "Checked by" field, make sure he's an editor or admin
	{	$role = get_user_role(get_user());
		if ($role != "editor" && $role != "admin") echo "<script language='javascript'>alert('You are not authorized to check records.')</script>";
		else
		{	$query1 = "UPDATE orgs SET checker = '".$value."' WHERE orgnum = '".$orgnum."'";	//If so, enter him as the checker; no other info needs recording.
			$result = mysqli_query($db,$query1);
		}
	}	else 																					
	{	$query4 = "UPDATE orgs SET ".$field." = '".$value."', changed_by= '".$user."', date_changed = '".date("y/m/d")."' WHERE orgnum = '".$orgnum."'";
		$result4 = mysqli_query($db,$query4);
		echo NEWLINE."<script language='javascript'>var next_field = '".$next."'  </script>".NEWLINE;   //$next is the id of the field to be selected when page reloads.
	}
	
	provide_org("orgnum",$orgnum);
}

function change_staff_field($field,$value,$email)
{	$db = db_connect();
	get_authorization("begin","000000");
	$query = "UPDATE staff set ".$field." = '".$value."' WHERE email = '".$email."'";
	$result = mysqli_query($db,$query);
	provide_staff();
}

function delete_term($field,$term)
{	$db = db_connect();
	get_authorization("begin","000000");
	$query = "DELETE FROM ".$field."terms WHERE ".$field."= '".$term."'";
	$result = mysqli_query($db,$query);
	provide_terms();	
}

function delete_org($orgnum)									//Deleting an org entails deleting it from all of the tables.
{	$db = db_connect();
	$user = get_user();
	$role = get_user_role($user);
	if ($role != "admin") 
	{	echo "<script language='javascript'>alert('You are not authorized to delete records.')</script>";
		provide_first_org();
	}	else
	{	$query1 = "DELETE FROM orgs WHERE orgnum = '".$orgnum."'";
		mysqli_query($db,$query1);
		$query2 = "DELETE FROM countys WHERE orgnum = '".$orgnum."'";
		mysqli_query($db,$query2);
		$query3 = "DELETE FROM contexts WHERE orgnum = '".$orgnum."'";
		mysqli_query($db,$query3);
		$query4 = "DELETE FROM zipcodes WHERE orgnum = '".$orgnum."'";
		mysqli_query($db,$query4);
		$query4 = "DELETE FROM categorys WHERE orgnum = '".$orgnum."'";
		mysqli_query($db,$query4);
		provide_first_org();
	}
}

function delete_search($date,$role,$zipcode,$context,$specifics1,$category,$specifics2,$specifics3)
{	$db = db_connect();
	$query = "DELETE FROM searches WHERE date = '".$date."' AND role = '".$role."' AND zipcode = '".$zipcode."' ";
	$query .= "AND context = '".$context."' AND specifics1 = '".$specifics1."' AND category = '".$category."' ";
	$query .= "AND specifics2 = '".$specifics2."' AND specifics3 = '".$specifics3."'";
	$result = mysqli_query($db,$query);
	provide_searches();
}


function delete_all_searches()
{	$db = db_connect();
	$query = "DELETE FROM searches";
	$result = mysqli_query($db,$query);
}

function delete_staff($email)
{	$db = db_connect();
	$query = "DELETE FROM staff WHERE email ='".$email."'";
	$result = mysqli_query($db,$query);
	provide_staff();
}

function delete_value($orgnum,$table,$field,$value,$keyword)
{	$db = db_connect();
	get_authorization("begin",$orgnum);
	$query = "DELETE FROM ".$table." WHERE orgnum = '".$orgnum."' AND ".$field." = '".$value."' ";
	if ($keyword != "") $query .= "AND specifier = '".$keyword."'";
	$result = mysqli_query($db,$query); 
	provide_org("orgnum",$orgnum);
}

function un_quote($expr)
{	return preg_replace('/"/','_',$expr);
}


//###############################//
//    MySQL EMAIL FUNCTIONS    //
//###############################//

function send_code($email)											//sends staff an email with a new pass code
{	$db = db_connect();
	$query = "SELECT * FROM staff WHERE email = '".$email."'";
	$result = mysqli_query($db,$query);
	if (mysqli_num_rows($result) == 0 || strspn(mysqli_result($email,0,'email'),"+") == 0)
	{	echo "<script language='javascript'>alert('That is not an authorized email address.');document.getElementById('staff').value=''</script>";
	}	else
	{	$newcode = get_passwd();
		$query2 = "UPDATE staff SET passwd = '".$newcode."' WHERE email = '".$email."'";
		$result2 = mysqli_query($db,$query2);
		send_email("From the NYCLU Hate Incident Reporting Database",$email,"Your new pass code is ".$newcode."Your new pass code is ".$newcode);
	}
}

function send_email($subject,$to,$message)
{	
	// Send the message
	
	$ok = mail($to, $subject, $message);
	if ($ok) echo '<script language="javascript">var sent = "sent"</script>';
	else echo '<script language="javascript">var sent = "failed"</script>';
}

?>
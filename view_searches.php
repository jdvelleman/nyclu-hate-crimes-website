<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />


<title>View Searches</title>
</head>

<body>
<div id="header"></div>
<table id="searches_table" style="background-color:white;width:1000px">
</table>
</body>
</html>
<script language="Javascript" src="functions.js">
</script>

<script language='javascript'>

function clearMe()
{	window.location = "view_searches.php?action=clear"
}

function deleteMe(which)
{	
	date = searches[which]['date']
	role = searches[which]['role']
	zipcode = searches[which]['zipcode']
	context = searches[which]['context']
	specifics1 = searches[which]['specifics1']
	category = searches[which]['category']
	specifics2 = searches[which]['specifics2']
	specifics3 = searches[which]['specifics3']
	
	newloc = "view_searches.php?action=delete&date=" + date + "&role=" + role + "&zipcode=" + zipcode + "&context=" + context
	newloc += "&specifics1=" + specifics1 + "&category=" + category + "&specifics2=" + specifics2 + "&specifics3=" + specifics3
	window.location = newloc
}

function fillTable()
{	theheader = "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DATE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>ROLE&nbsp;&nbsp;&nbsp;&nbsp;</td><td>ZIP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>"
	theheader += "<td>REGULATED AREA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>CONTEXT SPECIFICS&nbsp;&nbsp;&nbsp&nbsp;&nbsp&nbsp;</td>"
	theheader += "<td>PROTECTED CATEGORY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>"
	theheader += "<td>CATEGORY SPECIFICS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>ACT SPECIFICS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>"
	theheader += "<td><input type='button' id='clear_button' value='CLEAR ALL' onclick='clearMe()' /></td></tr>"
	document.getElementById("searches_table").innerHTML += theheader
	
	for (i=0; i<searches.length; i++)
	{	if (Math.round(i/2) == i/2) newrow = "<tr style='background-color:orange'>"
		else newrow = "<tr style='background-color:yellow'>"
		newrow += "<td>" + searches[i]['date'] + "</td>"
		newrow += "<td>" + searches[i]['role'] + "</td>"
		newrow += "<td>" + searches[i]['zipcode'] + "</td>"
		newrow += "<td>" + searches[i]['context'] + "</td>"
		newrow += "<td>" + searches[i]['specifics1'] + "</td>"
		newrow += "<td>" + searches[i]['category'] + "</td>"
		newrow += "<td>" + searches[i]['specifics2'] + "</td>"
		newrow += "<td>" + searches[i]['specifics3'] + "</td>"
		newrow += "<td><input type='button' value='delete' onclick='deleteMe(" + i + ")' </td></tr>"
		document.getElementById("searches_table").innerHTML += newrow
	}
}

</script>

<?php

require('db/NYCLUFUNCTIONS.php');

if (!get_param($action,'action')) 
{	get_authorization("begin","xxxxxx");
	provide_searches();
}
else if ($action == "getauth" && get_param($passwd,'passwd'))
{ 	get_authorization($passwd,'xxxxxx');
	provide_searches();
}	

else if ($action == "clear") delete_all_searches();
else if ($action == "delete" && get_param($date,'date') && get_param($role,'role') && get_param($zipcode,'zipcode') && get_param($context,'context') 
&& get_param($specifics1,'specifics1') && get_param($category,'category') && get_param($specifics2,'specifics2') && get_param($specifics3,'specifics3'))
delete_search($date,$role,$zipcode,$context,$specifics1,$category,$specifics2,$specifics3);


?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Edit Organizations</title>
</head>

<script language="Javascript" src="functions.js">
</script>

<script language="javascript">

var v_orgnum = "xxxxxx" //default values for variables to be re-defined below.
var next_field = "org"

/////////////////////
// NOTE ON QUOTES //
////////////////////
/*

Double quotes cannot be entered into text fields, so they will not appear in database. Apostrophes (hence also single quotes) are replaced by underline
and stored in that form. Underlines are replaced by apostrophes when data is provided.

*/

///////////////////////////////////////////
// FUNCTIONS AVAILABLE BEFORE PAGE LOADS //
//////////////////////////////////////////




</script>
<?php

require('db/NYCLUFUNCTIONS.php');

if (!get_param($action,'action')) 
{	provide_first_org();
}	else if ($action == 'newcode' && get_param($email,'email')) echo send_code($email); 					//sends user a new passcode
		else if ($action == "getauth" && get_param($orgnum,'orgnum') && get_param($passwd,'passwd')) get_authorization($passwd,$orgnum);  //called by testUser(); authenticates user
			else if ($action =="addorg" && get_param($org,'org') && get_param($type,'type'))
			{	add_org($org,$type);
				}	else if ($action == "addvalue" && get_param($orgnum,'orgnum') && get_param($field,'field') && get_param($value,'value'))
					{	add_value($orgnum,$field,$value);
					}	else if ($action == "find" && get_param($field,'field') && get_param($value,'value')) 
						{	provide_org($field,$value);														
						} 	else if ($action =="change" && get_param($orgnum,'orgnum') && get_param($field,'field') && get_param($value,'value') && get_param($next,'nextfield')) 
							{	change_field($orgnum,$field,$value,$next);
							} 	else if ($action == "delete" && get_param($orgnum,'orgnum') && get_param($table,'table') && get_param($field,'field') 
								&& get_param($value,'value') && get_param($keyword,'keyword'))
								{	delete_value($orgnum,$table,$field,$value,$keyword);
								} 	else if ($action == "drop" && get_param($orgnum,'orgnum'))
									{	delete_org($orgnum);
									}	else if ($action == "another" && get_param($orgnum,'orgnum') && get_param($direction,'direction')) 
										{	provide_another_org($orgnum,$direction);				
												}	else if ($action == "authanother" && get_param($orgnum,'orgnum') 
													&& get_param($author,'author') && get_param($direction,'direction'))
													{	provide_another_auth_org($author,$orgnum,$direction);
														} else if ($action == "unchecked") provide_unchecked();
															else if ($action == "specifics" && get_param($orgnum,'orgnum') && get_param($field,'field') 
																	&& get_param($value,'value') && get_param($keyword,'keyword'))
																{	add_specifics($orgnum,$field,$value,$keyword);
																} 	else if ($action == "initials" && get_param($value,'value')) 
																	{	test_initials($value);
																	}	else if ($action == "homepage" && get_param($orgnum,'orgnum')  && isset($_POST['homepage']))
																		{	change_field($orgnum,"homepage",$_POST['homepage'],"reportpage");	
																		}	else if ($action == "reportpage" && get_param($orgnum,'orgnum')  && isset($_POST['reportpage']))
																			{	change_field($orgnum,"reportpage",$_POST['reportpage'],"info");	
																			}	else if ($action == "info" && get_param($orgnum,'orgnum') && isset($_POST['info']))
																				{	change_field($orgnum,"info",$_POST['info'],"org");	
																				}


																				


provide_attribute_list("ziptocounty","county");		//These lists are the options for the county, context, and category menus
provide_attribute_list("contextterms","context");
provide_attribute_list("categoryterms","category");
//provide_attribute_list("orgs","org");	
					
						
?>


<body bgcolor="#E9E8FE">
<center><img src="nyclu-logo.png" /></center><br />

<table bgcolor="white" align="center">
<tr>
<td>
<table border="1" align="center">
<tr>
<td>
ID &nbsp;<input type="text" size="7" name="orgnum"  id="orgnum" disabled="disabled" >
Entered by:&nbsp;<input type="text" name="author" id="author" size="4" title="Password of author creating this record" />
<input type="submit" name="find_auth_orgs" id="find_auth_orgs" value="FIND" onclick="findAuth()" title="Click once for empty form; enter initials; click again to search."/>
 &nbsp;<input type="submit" name="previous_auth_org" id="previous_auth_org" value="<PREV" title="Previous record entered by author." onclick="anotherAuthOrg('prev')"/>
 &nbsp;<input type="submit" name="next_auth_org" id="next_auth_org" value="NEXT>" title="Next record entered by author" onclick="anotherAuthOrg('next')"/>&nbsp;&nbsp;Last edited on:&nbsp;
 <input type="text" name="edited_date" id="edited_date" size="12" disabled="disabled"/>
&nbsp;Checked by&nbsp;<input type="text" name="checker" id="checker" size="5" title = "Initials of person who signed off on the data" onChange="changeField(this)"/>
&nbsp;<input type="submit" name="next_auth_org" id="next_auth_org" value="CHECK NEXT" title="Click for record that needs checking." onclick="checkNext()"/>
</td>
</tr>
</table>
<br />   
<table bgcolor="white">
	<tr style="vertical-align:top"> 
		<td>
    <table>
     <tr>
    		<td>
            <div align="right">NAME  </div>
 			</td>
            <td style="width:550px"><input type="text" name="org" id="org" size="75" onChange="changeField(this)"/></td>
            <td></td>
    <tr>
            <td>
            </td>
            <td >
  	<input type="button" name="prev" id="prevprev" value="<PREV" onclick="anotherOrg('prev')"/>
    <input type="button" name="add" id="add" value="ADD" onclick="addOrg()" title="Click once for empty form; enter name of organization; click again to add."/>
    <input type="button" name="delete_org" id="delete_org" value="DELETE" onclick="deleteOrg()" />
    <input type="button" name="orglist" id="orglist" value="INDEX" onclick="show_orglist()" />
    <input type="button" name="next" id="next" value="NEXT>" align="right" onclick="anotherOrg('next')"/>
    		</td>
   <tr></tr>
    <tr>
            <td align="right">TYPE</td>
            <td>
    			<select name="type" id="type" onChange="changeField(this)">
    			<option value="">choose type</option>
    			<option value="reporting">reporting</option>
     			<option value="resource">resource</option>
    			</select>
                &nbsp;&nbsp;<input type="checkbox" id="nowitnesses" value="true" onChange="changeField(this)" />&nbspNO WITNESSES
    		</td>
            <td>
            </td>
    </tr>
    <tr>
        <td align="right">ATTN/Unit</td>
        <td><input type="text" name="unit" size="50" id="unit" onChange="changeField(this)" ></input></td>
        <td></td>
    </tr>
    <tr>
        <td align="right">Street 1 </td>
        <td><input type="text" name="street1" size="50" id="street1" onChange="changeField(this)" ></input></td>
        <td></td>
    </tr><tr>
        <td align="right">Street 2</td	>
        <td><input type="text" name="street2" size="50"  id="street2" onChange="changeField(this)" ></input></td>
        <td></td>
    </tr><tr>
        <td align="right">City</td>
        <td><input type="text" name="city" size="50"  id="city" onChange="changeField(this)" ></input></td>
        <td></td>
    </tr><tr>
        <td align="right">State</td>
        <td> <input type="text" name="state" size="3" id="state" onChange="changeField(this)" ></input></td>
        <td></td>
    </tr><tr>
        <td align="right">Zip</td>
        <td><input type="text" name="zip" size="7"  id="zip" onChange="changeField(this)" ></input></td>
        <td></td>
    </tr><tr>&nbsp;
        
    </tr><tr>
        <td align="right">Office</td>
        <td><input type="text" name="office" id="office" size="13" onChange="changeField(this)"/></td>
        <td></td>
    </tr><tr>
        <td align="right">Hotline</td>
        <td><input type="text" name="hotline" id="hotline" size="13" onChange="changeField(this)"/></td>
        <td></td>
    </tr>
        <td align="right">Fax</td>
        <td><input type="text" name="fax" id="fax" size="13" onChange="changeField(this)"/></td>
        <td></td>
    </tr><tr>
        <td align="right">Homepage</td>																<!-- these are forms because their values are too long for the query string -->
        <td align="left"><form id="homepage_form" name="homepage_form" method="post">				<!-- actions are assigned below, using v_orgnum -->
        				 <input type="text" name="homepage" id="homepage" size="75" onChange="this.parentNode.submit()"/>
                         </form></td>
        <td align="left"><input type="submit" name="open_homepage" id="test_homepage" value="open" onClick="openPage('home')"/>
        </td>
    </tr><tr>
        <td align="right">Report Page</td>
        <td align="left"><form id="reportpage_form" name="reportpage_form" method="post">
        				 <input type="text" name="reportpage" id="reportpage" size="75" onChange="this.parentNode.submit()"/>
                         </form></td>
        <td align="left"><input type="submit" name="test_reportpage" id="test_reportpage" value="open" onClick="openPage('report')" /></td>
    </tr>
    <tr>
    
    <td align="right">Information</td>
    <td><form id="info_form" name="info_form" method="post" >
   		 <textarea type="text" id="info" name="info" cols="75" rows="5" form="info_form" onChange="this.parentNode.submit()"></textarea>
         </form></td>
   
    <td></td>
    </table>
    <p></p>
       <table  border="0">
         <tr>
           <td width = "150px"><select name="choose_size" id="choose_size" onChange="chooseSize(this.value)">
           		<option value="choose" selected="true">choose size of region</option>
                <option value="zipcode">zipcodes</option>
                <option value="county">county or larger</option>
                </select> 
           </td>
           <td width = "500px">
           	 <select name="get_context" id="get_context" onChange="addValue(this,'context')"/>
           </td>
           <td width = "500px">
           	<select name="get_category" id="get_category" onChange="addValue(this,'category')"></select>
          </td>
           <td></td>
           
         </tr>
         <tr>
           	<td>
            <div id="get_region"></div>             
           </td>
           <td>
          	
           </td>
           <td>
            
             
           </td>
           <td></td>
           
         </tr>
         <tr>
           <td style="vertical-align:top;width:150px"><div name="list1" id="list1" ></div></td>
           <td style="vertical-align:top;width:250px"><div name="list2" id="list2" ></div></td>
           <td style="vertical-align:top;width:200px"><div name="list3" id="list3" ></div></td>
 
           <td></td>
         </tr>
          <tr>
           <td>
           </td>
           <td>
           </td>
           <td>
           </td>
           <td></td>
         </tr>
       </table>
       </td></tr>
 </table>
 
   <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>

</body>
</html>

<script language="javascript">

////////////////////////////////
//	EXECUTED WHEN PAGE LOADS  //
////////////////////////////////

document.addEventListener("keypress",keyCheck)

function keyCheck(event)
{	var key_id = (event.keyCode ? event.keyCode : event.which)
	if (key_id == "13") event.preventDefault()
}


context_menu = document.getElementById("get_context")					
	newopt = document.createElement("option")										
	newopt.text = "choose setting"
	newopt.value = ""
	context_menu.add(newopt)
	newopt = document.createElement("option")				//"All" is not in the lists of terms, becaause it isn't shown to users. It's added here.
		newopt.text = "All"
		newopt.value = "All"
		context_menu.add(newopt)
	for (i=0; i<context_list.length; i++)
	{	newopt = document.createElement("option")
		newopt.text = context_list[i]
		newopt.value = context_list[i]
		context_menu.add(newopt)
	}

category_menu = document.getElementById("get_category")
	newopt = document.createElement("option")
	newopt.text = "choose basis"
	newopt.value = ""
	category_menu.add(newopt)
	newopt = document.createElement("option")
	newopt.text = "All"
	newopt.value = "All"
	category_menu.add(newopt)
	for (i=0; i<category_list.length; i++)
	{	newopt = document.createElement("option")
		newopt.text = category_list[i]
		newopt.value = category_list[i]
		category_menu.add(newopt)
	}
	




	
if (v_orgnum != "xxxxxx")							//If this is not a blank form, fill in the org's info
{	if (countys.length > 0) chooseSize("county") 				//the user has chosen "county" as the size; set the choose_size menu and insert the county menu										
	else if (zipcodes.length > 0) chooseSize("zipcode")			//the user has chosen "zipcode" as the size; set the choose_size menu and insert "enter zipcode" field
	else document.getElementById("choose_size").selectedIndex = "0"	//regions have been entered, so size is still up for grabs.
	 
	for (i=0; i<countys.length; i++)		//fills in the list of countys that have been chosen for this agency
	{ 	thestuff = countys[i] + "&nbsp;<input type='submit' value='delete' "
		thestuff += "onClick='deleteValue(\"countys\",\"county\",\"" + countys[i] + "\")' /></br>"
		document.getElementById("list1").innerHTML += thestuff
	}
	
				
	for (i=0; i<zipcodes.length; i++)		//fills in the list of zipcodes that have been chosen for this agency
	{	thestuff = zipcodes[i] + "&nbsp;<input type='submit' value='delete' "
		thestuff += "onClick='deleteValue(\"zipcodes\",\"zipcode\",\"" + zipcodes[i] + "\")' /></br>"  
		document.getElementById("list1").innerHTML += thestuff
	}
	
	
	
	for (i=0; i<contexts.length; i++)		//fills in the list of contexts. Those which do not have keywords associated with them are followed by field, button to add one.	
	{	if (!Array.isArray(contexts[i]))	//context-keyword pairs are sub-arrays.
		{	thestuff = contexts[i] + "&nbsp;<input type='submit' value='delete' onClick='deleteValue(\"contexts\",\"context\",\"" + contexts[i] + "\")' />"
			thestuff += "&nbsp<input type='text' size='10' id = 'context_" + i + "' onFocus='this.select()' value='keyword'>"
			thestuff += "<input type=button value='ADD' onclick = 'addSpecifics(\"context\",\"" + i + "\")'>"			
		}	else 
		{	thestuff = contexts[i] + "&nbsp;<input type='submit' value='delete' onClick='deleteValue(\"contexts\",\"context\",\"" + contexts[i] + "\")' />"
		}
		document.getElementById("list2").innerHTML += thestuff + "</br>"
	}
	
	for (i=0; i<categorys.length; i++)		//fills in the list of categories. Those which do not have keywords associated with them are followed by field, button to add one
	{	if (!Array.isArray(categorys[i]))	//category-keyword pairs are sub-arrays.
		{	thestuff = categorys[i] + "&nbsp;<input type='submit' value='delete' onClick='deleteValue(\"categorys\",\"category\",\"" + categorys[i] + "\")' />"
			thestuff += "&nbsp<input type='text' size='10' id='category_" + i + "' value='keyword'>"
			thestuff += "<input type=button value='ADD' onFocus = 'this.select()' 'addSpecifics(\"category\",\"" + i + "\")'>"
			
		}	else 
		{	thestuff = categorys[i] + "&nbsp;<input type='button' value='delete' "
			thestuff += "onClick='deleteValue(\"categorys\",\"category\",\"" + categorys[i] + "\")' />"
		}	
		document.getElementById("list3").innerHTML += thestuff + "</br />"
		
		
	}
	
		document.getElementById("org").value = reQuote(v_org)							//fills in the values passed by provide_org(); see NOTE ON QUOTES at top
		document.getElementById("unit").value = reQuote(v_unit)
		document.getElementById("orgnum").value = v_orgnum
		document.getElementById("street1").value = reQuote(v_street1)
		document.getElementById("street2").value = reQuote(v_street2)
		document.getElementById("city").value = reQuote(v_city)
		document.getElementById("state").value = reQuote(v_state)
		document.getElementById("zip").value = reQuote(v_zip)
		document.getElementById("office").value = reQuote(v_office)
		document.getElementById("hotline").value = reQuote(v_hotline)
		document.getElementById("fax").value = reQuote(v_fax)
		document.getElementById("homepage").value = reQuote(v_homepage)
		document.getElementById("reportpage").value = reQuote(v_reportpage)
		document.getElementById("author").value = reQuote(v_author)
		document.getElementById("checker").value = reQuote(v_checker)
		document.getElementById("edited_date").value = reQuote(v_date)
		document.getElementById("info").value = reQuote(v_info)
		document.getElementById("type").selected = document.getElementById("type").selectedIndex = ["","reporting","resource"].indexOf(v_type)
		document.getElementById("nowitnesses").checked = eval(v_nowitnesses)
		document.getElementById("author").value = reQuote(v_author)
		document.getElementById("homepage_form").action = "edit_orgs.php?action=homepage&orgnum=" + v_orgnum
		document.getElementById("reportpage_form").action = "edit_orgs.php?action=reportpage&orgnum=" + v_orgnum
		document.getElementById("info_form").action = "edit_orgs.php?action=info&orgnum=" + v_orgnum
}
	
document.getElementById(next_field).select()

/*document.onkeypress = function(evt) 							//from http://stackoverflow.com/questions/1846599/how-to-find-out-what-character-key-is-pressed
{	evt = evt || window.event;									//prevents use of quotation marks in data
    var charCode = evt.keyCode || evt.which;
    var charStr = String.fromCharCode(charCode);
    if (charStr == '\"') 
	{	alert("Do not use quotation marks, please.")
		evt.target.value = evt.target.value.slice(0,evt.target.length-1)
		evt.target.select()
	}
}
*/

////////////////////////////////
//	       FUNCTIONS		  //
////////////////////////////////

function addOrg()
{  	org =	document.getElementById("org").value
	if (document.getElementById("orgnum").value != "") 				//If there's an orgnum, we're looking at an existing org...
	{	window.location = "edit_orgs.php?action=blank"																//...so this click is a request for a blank to enter a new org.
	}	else														//Otherwise, we're looking at a new org, and we need to add it.
	{	if (org == "")
			{	alert ("Please enter the name of the agency.")
			}	else 
			{	if (document.getElementById("type").selectedIndex == 0) 
				{	alert ("Please indicate the type of agency.")
				}	else
				{	newloc = "edit_orgs.php?action=addorg&org=" + org
					newloc += "&type=" + document.getElementById("type").value
					window.location = unQuote(newloc)
				}
			}
	}
}

function addSpecifics(field,fieldnum)
{	thefield = document.getElementById(field + "_" + fieldnum)
	if (checkFields(thefield))
	{	keyword = unQuote(thefield.value)
		thestuff = "edit_orgs.php?action=specifics&orgnum=" + v_orgnum + "&field=" + field + "&value=" + eval(field + "s[" + fieldnum + "]") 
		thestuff += "&keyword=" + keyword
		window.location = thestuff
	}
}

function addValue(source,field)
{	value = source.value
	if (value != "choose region" && value != "" && value != "choose context" && value != "choose category")
	{	if ((value == "state" || value == "nation") && (countys[0] == "state" || countys[0] == "nation"))
		{	alert ("Redundant!")
		} else	window.location="edit_orgs.php?action=addvalue&orgnum=" + v_orgnum + "&field=" + field + "&value=" + value
	}
}

function escapeURL(url)
{	lastslash = url.lastIndexOf("/")
	firsthalf = url.slice(0,lastslash)
	secondhalf = url.slice(lastslash + 1,url.length)
	return firsthalf + escape(lasthalf)
}

function anotherAuthOrg(direction)		//retrieves the previous or next agency entered by the current author
{	window.location = "edit_orgs.php?action=authanother&author=" + document.getElementById("author").value + "&orgnum=" + v_orgnum + "&direction=" + direction
}

function anotherOrg(direction)		//retrieves the previous or next agency in the database, in alphabetic order
{	window.location = "edit_orgs.php?action=another&orgnum=" + v_orgnum + "&direction=" + direction
}

function changeField(which)								
{	field = which.id
	if (checkFields(which))
	{ 	field_nodes = Array.prototype.slice.call(document.getElementsByTagName("input"))			//an array of all the childNodes of the documen
		if (document.getElementById("orgnum").value == "")   	//If this is a blank form...	
		{	if (which.id != "org" && which.id != "type")		//... don't let the user try to enter more information that what is needed to add an org ...
				{	alert ("You cannot enter this information until you have added the agency to the database")
				}																		//... but otherwise do nothing. Fields can't be changed until the org has been added; let
		}	else																		//If this is the record of an existing org
		{	
				{	thisnode = field_nodes.indexOf(which)
					if (thisnode < field_nodes.length - 2) next_field = field_nodes[thisnode + 1].id
					if (field == "nowitnesses") value = which.checked
						else if (field == "homepage" || field == "reportpage") value = escapeURL(which.value)
							else value = which.value
					newloc = "edit_orgs.php?action=change&orgnum=" + v_orgnum + "&field=" + field + "&value=" + value 
					newloc += "&nextfield=" + next_field 								//Now execute the change.
					window.location = unQuote(newloc)
				}
		}
	}
}

function checkFields(which)
{	if ((which.id == "office" || which.id == "hotline" || which.id == "fax") && which.value != "" && which.value.match(/^[0-9]{3}\-[0-9]{3}\-[0-9]{4}$/) != which.value)
	{	except = confirm ("Telephone numbers should have the format: ###-###-####, though there are exceptions. Is this an exception?")
		return except
	} else if (which.value.indexOf('"') >= 0) 
		{	alert ("Do not use quotation marks")
			return false
		}	else if ((which.id == "homepage" || which.id == "reportpage") && which.value.indexOf("http") >= 0) 
			{	alert ("Don't include 'http://'")
				return false
			}	else if (which.id == "state" && which.value.length != 2 && which.value.match(/^[ACDEFGHIJKLMNOPRSTUVWXYZ]{2}$/) != which.value)
				{	alert("That's not a valid state abbreviation.")
					return false
				}	else return true
}


function checkNext()			//retrieves the next record that hasn't been checked.
{	window.location = "edit_orgs.php?action=unchecked";
}

function chooseSize(what)
{	document.getElementById("choose_size").selectedIndex = ["","zipcode","county"].indexOf(what) //sets the choose_size 
	thediv = document.getElementById("get_region")
	if (what == "zipcode")
	{	if (countys.length > 0) 
		{	alert("Choose either counties or zipcodes -- not both.")
			document.getElementById("choose_size").selectedIndex = "2"
		}	else
		{	while (thediv.hasChildNodes()) thediv.removeChild(thediv.childNodes[0])
			zip_field = document.createElement("input")
			zip_field.type = "text"
			zip_field.id = "zipcode"
			zip_field.size = "8"
			zip_field.value = "zipcode"
			zip_field.onclick = function(){this.select()}
			thediv.appendChild(zip_field)
			zip_button = document.createElement("input")
			zip_button.type = "submit"
			zip_button.value = "ADD"
			zip_button.onclick = function(){addValue(document.getElementById("zipcode"),"zipcode")}
			thediv.appendChild(zip_button)
		}
	} else if (what == "county")
	{	if (zipcodes.length > 0) 
		{	alert ("Choose either zipcodes or counties -- not both.")
			document.getElementById("choose_size").selectedIndex = "1"
		}	else
		{	while (thediv.hasChildNodes()) thediv.removeChild(thediv.childNodes[0])
			county_menu = thediv.appendChild(document.createElement("select"))
			county_menu.id = "county_menu"
			county_menu.onchange = function(){addValue(this,"county")}
			newopt = document.createElement("option")
			newopt.text = "choose region"
			newopt.value = ""
			county_menu.add(newopt)
			newopt = document.createElement("option")
			newopt.text = "Nation-wide"
			newopt.value = "nation"
			county_menu.add(newopt)
			newopt = document.createElement("option")
			newopt.text = "State-wide"
			newopt.value = "state"
			county_menu.add(newopt)
			for (i=0; i<county_list.length; i++)
			{	newopt = document.createElement("option")
				newopt.text = county_list[i]
				newopt.value = county_list[i]
				county_menu.add(newopt)
			}
		}
	}
}

function deleteOrg()
{	if (confirm("Are you sure you want to delete this organization?"))
	{ if (confirm("This action is not reversible. Continue with delete?"))
		{	window.location = "edit_orgs.php?action=drop&orgnum=" + v_orgnum
		}
	}
}

function deleteValue(source,field,value)
{	keyword = ""	
	comma = value.indexOf(",")													//If the context or category has a keyword, must be treated as an array
	if (comma >= 0) 
	{	keyword = value.slice(comma+1,value.length)
		value = value.slice(0,comma)
	}
	newloc = "edit_orgs.php?action=delete&orgnum=" + v_orgnum + "&table=" + source + "&field=" + field + "&value=" + value + "&keyword=" + keyword
	window.location = newloc
}

function findAuth()						//retrieves the most recent record entered by a author  
{	if (document.getElementById("orgnum").value != "") window.location = "edit_orgs.php?action=blank" //the first click brings up a blank, for author passcode to be filled in
	else if (document.getElementById("author").value == "") alert("Please enter author initials.")
	else window.location = "edit_orgs.php?action=find&field=author&value=" + document.getElementById("author").value
}

function findOrg()
{	if (document.getElementById("orgnum").value != "") window.location = "edit_orgs.php?action=blank"
	else if (document.getElementById("org").value != "")  
	{	window.location="edit_orgs.php?action=find&field=org" + "&value="  + document.getElementById("org").value
	}  else alert ("Enter organization name .")
}

function openPage(which)
{	address = document.getElementById(which + "page").value
	if (address.indexOf("http://" < 0 )) window.location = "http://" + address
	else testwindow = window.new(address)
}

function show_orglist()
{	window.location="listwindow.php"
}






</script>


